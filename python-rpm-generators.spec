%undefine py_auto_byte_compile

Name: python-rpm-generators
Version: 9
Release: 5
Summary: Dependency generators for Python RPMs

License: GPLv2+
URL: https://src.fedoraproject.org/rpms/python-rpm-generators
Source0: https://raw.githubusercontent.com/rpm-software-management/rpm/102eab50b3d0d6546dfe082eac0ade21e6b3dbf1/COPYING
Source1: python.attr
Source2: pythondist.attr
Source3: pythondeps.sh
Source4: pythondistdeps.py

BuildArch: noarch

%description
Fedora's dependency generators for Python RPMS.

%package -n python3-rpm-generators
Summary: %{summary}
Requires: python3-setuptools
Conflicts: rpm-build < 4.13.0.1-2
Conflicts: python-rpm-macros < 3-35

%description -n python3-rpm-generators
Fedora's dependency generators for Python RPMS.

%prep
%autosetup -c -T
cp -a %{sources} .

%install
install -Dpm0644 -t %{buildroot}%{_fileattrsdir} python.attr pythondist.attr
install -Dpm0755 -t %{buildroot}%{_rpmconfigdir} pythondeps.sh pythondistdeps.py

%files -n python3-rpm-generators
%defattr(-,root,root)
%license COPYING
%{_fileattrsdir}/python.attr
%{_fileattrsdir}/pythondist.attr
%{_rpmconfigdir}/pythondeps.sh
%{_rpmconfigdir}/pythondistdeps.py

%changelog
* Tue Aug 06 2024 chenhuihan <chenhuihan@huawei.com> - 9-5
- Fix python provide and requires

* Sun Apr 23 2023 caodongxia <caodongxia@h-partners.com> - 9-4
- Fix the python3 version cannot be found when pyproject.toml is used for building

* Thu Oct 27 2022 zhangruifang <zhangruifang1@h-partners.com> - 9-3
- Rebuild for next release

* Fri Jan 07 2022 shixuantong <shixuantong@huawei.com> - 9-2
- downgrade version to 9

* Thu Dec 09 2021 liudabo <liudabo1@huawei.com> - 12-1
- upgrade version to 12

* Tue Sep 17 2019 openEuler Buildteam <buildteam@openeuler.org> - 9-1
- Package init
